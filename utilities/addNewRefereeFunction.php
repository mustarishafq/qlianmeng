<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

$uid = $_SESSION['uid'];

// function registerNewUser($conn,$uid,$username,$fullname,$email,$finalPassword,$salt,$icNo,$points,$referrerUid = null,$topReferrerUid = null,$referralName = null,$currentLevel = null)
function registerNewUser($conn,$uid,$username,$fullname,$email,$finalPassword,$salt,$points,$referrerUid = null,$topReferrerUid = null,$referralName = null,$currentLevel = null)
{
$isReferred = 0;
if($referrerUid && $topReferrerUid)
{
$isReferred = 1;
}

// if(insertDynamicData($conn,"user",array("uid","username","full_name","email","password","salt","ic_no","bonus","is_referred"),
//      array($uid,$username,$fullname,$email,$finalPassword,$salt,$icNo,$points,$isReferred),"ssssssssi") === null)
// if(insertDynamicData($conn,"user",array("uid","username","full_name","email","password","salt","bonus","is_referred"),
//      array($uid,$username,$fullname,$email,$finalPassword,$salt,$points,$isReferred),"sssssssi") === null)
if(insertDynamicData($conn,"user",array("uid","username","full_name","email","password","salt","point","is_referred"),
     array($uid,$username,$fullname,$email,$finalPassword,$salt,$points,$isReferred),"sssssssi") === null)
{
     header('Location: ../addReferee.php?promptError=1');
     //     promptError("error registering new account.The account already exist");
     //     return false;
}
else
{
     if($isReferred === 1)
     {
     if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
          array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis") === null)
          {
               header('Location: ../addReferee.php?promptError=2');
               //   promptError("error assigning referral relationship");
               //   return false;
          }
     }
}
return true;
}

function sendEmailForVerification($uid) 
{
     $conn = connDB();
     $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

     $verifyUser_debugMode = 2;
     $verifyUser_host = "mail.qlianmeng.asia";
     $verifyUser_usernameThatSendEmail = "noreply@qlianmeng.asia";                   // Sender Acc Username
     $verifyUser_password = "P&=?^l=j-g5o";                                                      // Sender Acc Password
     $verifyUser_smtpSecure = "ssl";                                                           // SMTP type
     $verifyUser_port = 465;                                                                   // SMTP port no
     $verifyUser_sentFromThisEmailName = "noreply@qlianmeng.asia";                                       // Sender Username
     $verifyUser_sentFromThisEmail = "noreply@qlianmeng.asia";                       // Sender Email
     $verifyUser_sendToThisEmailName = $userRows[0]->getUsername();                                      // Recipient Username
     $verifyUser_sendToThisEmail = $userRows[0]->getEmail();                                   // Recipient Email
     $verifyUser_isHtml = true;                                                                // Set To Html
     $verifyUser_subject = "Welcome to Q LianMeng";                                      // Title

     // $verifyUser_body = "<p>尊敬的用户，</p>"; // Body
     // $verifyUser_body .="<p>恭喜你！ 您的帐户已成功创建并激活。</p>";
     // $verifyUser_body .="<p>请确保此登录信息的安全，因为您将使用它登录qlianmeng。</p>";
     // $verifyUser_body .="<p>登录名 : ".$userRows[0]->getUsername()."</p>";
     // $verifyUser_body .="<p>密码 ：123321</p>";
     // $verifyUser_body .="<p>登录帐户后，请在个人资料部分更改密码。</p>";
     // $verifyUser_body .="<p>谨祝问候，</p>";
     // $verifyUser_body .="<p>qlianmeng</p>";

     $verifyUser_body = "<p>Dear user,</p>"; // Body
     $verifyUser_body .="<p>Congratulations, your account has been successfully created !</p>";
     $verifyUser_body .="<p>Below is the details to login to the website.</p>";
     $verifyUser_body .="<p>http://www.qlianmeng.asia</p>";
     $verifyUser_body .="<p>Username : ".$userRows[0]->getUsername()."</p>";
     $verifyUser_body .="<p>Password ：123321</p>";
     $verifyUser_body .="<p>For safety purpose, we encourage you to change your password after first time login.</p>";
     $verifyUser_body .="<p>Best Regards,</p>";
     $verifyUser_body .="<p>qlianmeng</p>";
     
          sendMailTo(
               null,
               $verifyUser_host,
               $verifyUser_usernameThatSendEmail,
               $verifyUser_password,
               $verifyUser_smtpSecure,
               $verifyUser_port, 
               $verifyUser_sentFromThisEmailName,
               $verifyUser_sentFromThisEmail,
               $verifyUser_sendToThisEmailName,
               $verifyUser_sendToThisEmail,
               $verifyUser_isHtml,
               $verifyUser_subject,
               $verifyUser_body,
               null
          );
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $register_uid = md5(uniqid());

     $register_username = rewrite($_POST['register_username']);
     $register_fullname = rewrite($_POST['register_fullname']);
     // $register_ic_no = rewrite($_POST['register_ic_no']);
     $register_email_user = $_POST['register_email_user'];

     $register_username_referrer = $_POST['register_username_referrer'];

     $register_password = $_POST['register_password'];
     $register_password_validation = strlen($register_password);
     $register_retype_password = $_POST['register_retype_password'];
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     $current_amount = $_POST['current_amount'];
     $addUserCharges = 1980;
     $charges = 1*(30/100);

     $final_amount = $current_amount - $addUserCharges;     //to upline  registerBonus
     $commission =$addUserCharges * $charges;               //to upline  shippingBonus

     $newUser = $addUserCharges;                            //to downline  shippingBonus

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $register_uid."<br>";

          if($register_password == $register_retype_password)
          {
               if($register_password_validation >= 6)
               {
                    if($register_username_referrer)
                    {
                         $referrerUserRows = getUser($conn," WHERE username = ? ",array("username"),array($register_username_referrer),"s");

                         if($referrerUserRows)
                         {
                              $referrerUid = $referrerUserRows[0]->getUid();
                              $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
                              $referralName = $register_username;
                              $currentLevel = 1;
                  
                              $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");

                              if($referralHistoryRows)
                              {
                                  $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                                  $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
                              }

                              $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
                              $usernameDetails = $usernameRows[0];

                              $fullnameRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($_POST['register_fullname']),"s");
                              $fullnameDetails = $fullnameRows[0];

                              $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email_user']),"s");
                              $userEmailDetails = $userEmailRows[0];

                              // if (!$usernameDetails && !$fullnameDetails && !$icNoDetails && !$userEmailDetails)
                              if (!$usernameDetails && !$fullnameDetails && !$userEmailDetails)
                              {
                                   $register_points = $newUser;  

                                   // if(registerNewUser($conn,$register_uid,$register_username,$register_fullname,$register_email_user,$finalPassword,$salt,$register_ic_no,$register_points,$referrerUid,$topReferrerUid,$referralName,$currentLevel))
                                   if(registerNewUser($conn,$register_uid,$register_username,$register_fullname,$register_email_user,$finalPassword,$salt,$register_points,$referrerUid,$topReferrerUid,$referralName,$currentLevel))
                                   {

                                        $uplineRows = getUser($conn," WHERE username = ? ",array("username"),array($register_username_referrer),"s");
                                        $uplineDetails = $uplineRows[0];

                                        $registerPoints = $uplineDetails->getBonus();
                                        // $shippingPoints = $uplineDetails->getUserPoint();
                                        $shippingPoints = $uplineDetails->getPoint();

                                        $uplineUid = $uplineDetails->getUid();
                                        $uplineUsername = $uplineDetails->getUsername();

                                        $uplineRegisterDownlineNo = $uplineDetails->getRegisterDownlineNo();
                                        $newUplineRegisterDownlineNo = $regisuplineRegisterDownlineNoterPoints + 1;

                                        $uplineRegisterPoints = $registerPoints - $addUserCharges;
                                        $uplineShippingPoints = $shippingPoints + $commission;

                                        $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                        if(!$user)
                                        {
                                             $tableName = array();
                                             $tableValue =  array();
                                             $stringType =  "";
                                             //echo "save to database";
                                             if($uplineRegisterPoints)
                                             {
                                                  array_push($tableName,"bonus");
                                                  array_push($tableValue,$uplineRegisterPoints);
                                                  $stringType .=  "s";
                                             }
                                             if(!$uplineRegisterPoints)
                                             {
                                                  $uplineRegisterPoints = 0;
                                                  array_push($tableName,"bonus");
                                                  array_push($tableValue,$uplineRegisterPoints);
                                                  $stringType .=  "s";
                                             }
                                             if($uplineShippingPoints)
                                             {
                                                  array_push($tableName,"point");
                                                  array_push($tableValue,$uplineShippingPoints);
                                                  $stringType .=  "s";
                                             }
                                             if($newUplineRegisterDownlineNo)
                                             {
                                                  array_push($tableName,"register_downline_no");
                                                  array_push($tableValue,$newUplineRegisterDownlineNo);
                                                  $stringType .=  "s";
                                             }

                                             array_push($tableValue,$register_username_referrer);
                                             $stringType .=  "s";
                                             $registerPointUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                             if($registerPointUpdated)
                                             { 

                                                  $referrerUid = $uplineUid;
                                                  $referrerName = $uplineUsername;

                                                  $referralUid = $register_uid;
                                                  $referralName = $register_username;
                                                  $referralFullname = $register_fullname;

                                                  $uplineCommission = $commission;

                                                  if (commission($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$uplineCommission))
                                                       {
                                                            // echo "success";
                                                       }
                                                  else
                                                  {}
                                             }
                                             else
                                             { }
                                        }

                                        sendEmailForVerification($register_uid);
                                        $_SESSION['messageType'] = 1;
                                        // header('Location: ../addReferee.php?type=1');
                                        header('Location: ../profile.php?type=1');
                                   }
                              }
                              else
                              {
                                   header('Location: ../addReferee.php?promptError=1');
                              }
                         }
                         else
                         {
                              $_SESSION['messageType'] = 1;
                              header('Location: ../addReferee.php?type=5');
                         }
                    }
                    else 
                    { }
               }
               else 
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../addReferee.php?type=5');
               }
          }
          else 
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../addReferee.php?type=5');
          }   
    
}
else 
{
     header('Location: ../addReferee.php');
}

function commission($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$commission)
{
     if(insertDynamicData($conn,"signup_commission",array("referrer_id","referrer_name","referral_id","referral_name","referral_fullname","commission"),
     array($referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$commission),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

?>