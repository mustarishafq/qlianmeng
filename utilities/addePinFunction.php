<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();
    $uid  = $_SESSION['uid'];

    $register_epin = $_POST['register_epin'];
    $register_epin_validation = strlen($register_epin);
    $register_epin_reenter = $_POST['register_epin_reenter'];

    $epin = hash('sha256',$register_epin);
    $salt1 = substr(sha1(mt_rand()), 0, 100);
    $finalePin = hash('sha256', $salt1.$epin);
    
    // echo $register_epin."<br>";
    // echo $salt1."<br>";
    // echo $finalePin."<br>";
    
    $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");   

    //$passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("password","salt"),array($finalPassword,$salt,$uid),"sss");

    if(!$user)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($finalePin)
            {
                array_push($tableName,"epin");
                array_push($tableValue,$finalePin);
                $stringType .=  "s";
            }
            if($salt1)
            {
                array_push($tableName,"salt_epin");
                array_push($tableValue,$salt1);
                $stringType .=  "s";
            }
           
            array_push($tableValue,$uid);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                // echo "success";
                $_SESSION['messageType'] = 3;
                header('Location: ../profile.php?type=1');
                // echo "<br>";
                // echo $salt1."<br>";
                // echo $finalePin."<br>";
            }
            else
            {
                // echo "fail";
                $_SESSION['messageType'] = 3;
                header('Location: ../profile.php?type=2');
            }
        }
        else
        {
            //echo "";
            $_SESSION['messageType'] = 3;
            header('Location: ../profile.php?type=3');
        }
}
?>