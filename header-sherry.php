    <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="left-logo-div float-left hidden-logo-padding">
                <a href="index.php"><img src="img/logo.png" class="logo-img" alt="Q联盟" title="Q联盟"></a>
            </div>
            <?php

            if(isset($_SESSION['usertype_level']) && $_SESSION['usertype_level'] == 1)
            {
                ?>
                 <div class="middle-menu-div float-left hide">
                    <a href="index.php#about" class="menu-padding white-to-yellow">About</a>
                    <a href="index.php#products" class="menu-padding white-to-yellow">Products</a>
                    <a href="index.php#contact" class="menu-padding white-to-yellow">Contact Us</a>
                    <a href="faq.php" class="menu-padding white-to-yellow">FAQ</a>
                </div>

                <?php
                if(isset($_SESSION['uid']))
                {
                    // echo count($_SESSION);
                    ?>
                    <div class="right-menu-div float-right" id="after-login-menu">
                        <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/white-user.png" class="cart-img" alt="个人中心" title="个人中心"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="profile.php"  class="menu-padding dropdown-a"><img src="img/user-black.png" class="cart-img dropdown-img2" alt="个人中心" title="个人中心"> 个人中心</a></p>
                                <!-- <p class="dropdown-p"><a href="editProfile.php"  class="menu-padding dropdown-a"><img src="img/edit-profile.png" class="cart-img dropdown-img2" alt="Edit Profile" title="Edit Profile"> Edit Profile</a></p> -->
                                <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a"><img src="img/password-d.png" class="cart-img dropdown-img2" alt="更改密码" title="更改密码"> 更改密码</a></p>
                                <p class="dropdown-p"><a href="referee.php" class="menu-padding dropdown-a"><img src="img/user-group.png" class="cart-img dropdown-img2" alt="我的组织成员" title="我的组织成员"> 我的组织成员</a></p>
                                <p class="dropdown-p"><a href="addReferee.php" class="menu-padding dropdown-a"><img src="img/add-referee.png" class="cart-img dropdown-img2" alt="添加新会员" title="添加新会员"> 添加新会员</a></p>
                            </div>
                        </div>
   
                        <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/document.png" class="cart-img" alt="Report" title="Report"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><b>报告</b></p>
                                <p class="dropdown-p"><a href="transferPointReport.php"  class="menu-padding dropdown-a"><img src="img/transfer-report.png" class="cart-img dropdown-img2" alt="转让分数报告" title="转让分数报告"> 转让运输分数报告</a></p>
                                <p class="dropdown-p"><a href="registerPointReport.php"  class="menu-padding dropdown-a"><img src="img/transfer-report.png" class="cart-img dropdown-img2" alt="转让注册分数报告" title="转让注册分数报告"> 转让注册分数报告</a></p>
                                <p class="dropdown-p"><a href="signUpReport.php"  class="menu-padding dropdown-a"><img src="img/transfer-report.png" class="cart-img dropdown-img2" alt="注册新会员报告" title="注册新会员报告"> 注册新会员报告</a></p>
                                <p class="dropdown-p"><a href="signUpCommissionReport.php"  class="menu-padding dropdown-a"><img src="img/transfer-report.png" class="cart-img dropdown-img2" alt="注册新会员分数报告" title="注册新会员分数报告"> 注册新会员分数报告</a></p>
                            </div>
                        </div>

                        <a href="logout.php"  class="menu-padding"><img src="img/logout-white.png" class="cart-img" alt="登出" title="登出"></a>

					<div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">打开菜单</button>
						<ul class="dl-menu">
                                  <li class="title-li">个人</li>
                                  <li><a href="profile.php" class="mini-li">个人中心</a></li>
                                  <!-- <li><a href="editProfile.php"  class="mini-li">Edit Profile</a></li> -->
                                  <li><a href="editPassword.php"  class="mini-li">更改密码</a></li>
                                  <li class="title-li">我的组织</li>
                                  <li><a href="referee.php" class="mini-li">我的组织成员</a></li>
                                  <li><a href="addReferee.php" class="mini-li">添加新会员</a></li>

                                  <li class="title-li">报告</li>
                                  <li><a href="transferPointReport.php" class="mini-li">转让运输分数报告</a></li>
                                  <li><a href="registerPointReport.php" class="mini-li">转让注册分数报告</a></li>
                                  <li><a href="signUpReport.php" class="mini-li">注册新会员报告</a></li>
                                  <li><a href="signUpCommissionReport.php" class="mini-li">注册新会员分数报告</a></li>
                                  <!-- <li class="title-li"><a href="logout.php">Logout</a></li> -->
                                  <li  class="title-li last-li"><a href="logout.php">登出</a></li>
						</ul>
					</div>

                    </div>
                    <?php
                }
                else
                {
                    ?>
                    <div class="right-menu-div float-right">
                    	<!--<a href="faq.php" class="menu-padding white-to-yellow faq-padding">FAQ</a>-->
                        <a  class="menu-padding white-to-yellow div-div open-login">登入</a>
                        <a class="open-menu"><img src="img/white-menu.png" class="menu-icon"></a>
                    </div>
                    <?php
                }
            }
            else
            {
                if(isset($_SESSION['uid']))
                {
                ?>
                <!-- <div class="middle-menu-div right-menu-div1"> -->
                <div class="right-menu-div float-right" id="after-login-menu">
                    <a href="adminDashboard.php" class="menu-padding spacing-a white-to-yellow white-link">概览</a>
                    <a href="adminMember.php" class="menu-padding spacing-a white-to-yellow white-link">会员</a>

                    <div class="dropdown hover1">
                        
                        <a  class="menu-padding dropdown-btn white-link">操作
                            <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                            <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                        </a>
                        <div class="dropdown-content yellow-dropdown-content">
                            <p class="dropdown-p"><a href="adminAddReferee.php"  class="menu-padding dropdown-a"><img src="img/add-referee.png" class="cart-img dropdown-img2" alt="添加会员" title="添加会员">添加会员</a></p>
                            <p class="dropdown-p"><a href="adminTransferPoint.php"  class="menu-padding dropdown-a"><img src="img/points.png" class="cart-img dropdown-img2" alt="转让运输分数" title="转让运输分数">转让运输分数</a></p>
                            <p class="dropdown-p"><a href="adminTransferRegisterPoint.php"  class="menu-padding dropdown-a"><img src="img/register-point2.png" class="cart-img dropdown-img2" alt="转让注册分数" title="转让注册分数">转让注册分数</a></p>
                        </div>
                    </div>    

                    <div class="dropdown hover1">

                        <a  class="menu-padding white-link">报告</a>
                        <a  class="menu-padding dropdown-btn">
                            <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                            <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                        </a>

                        <div class="dropdown-content yellow-dropdown-content">
                            <p class="dropdown-p"><a href="adminRegisterPointReport.php"  class="menu-padding dropdown-a"><img src="img/register-point2.png" class="cart-img dropdown-img2" alt="转让注册分数报告" title="转让注册分数报告">转让注册分数报告</a></p>
                            <p class="dropdown-p"><a href="adminTransferPointReport.php"  class="menu-padding dropdown-a"><img src="img/transfer-report.png" class="cart-img dropdown-img2" alt="转让运输分数报告" title="转让运输分数报告">转让运输分数报告</a></p>
                            <p class="dropdown-p"><a href="adminSignUpReport.php"  class="menu-padding dropdown-a"><img src="img/user-group.png" class="cart-img dropdown-img2" alt="公司注册新户口报告" title="公司注册新户口报告">公司注册新户口报告</a></p>
                        </div>
                    </div>    

                    <a href="adminEditPassword.php" class="menu-padding white-to-yellow white-link spacing-a">更换密码</a>
                    <a href="logout.php"  class="menu-padding white-to-yellow white-link spacing-a">登出</a>
                </div>

                    <div class="float-right right-menu-div">
                          	<div id="dl-menu" class="dl-menuwrapper">
                                <button class="dl-trigger">打开菜单</button>
                                <ul class="dl-menu">
                                    <li><a href="adminDashboard.php">概览</a></li>
                                    <li><a href="adminMember.php">会员</a></li>
                                    <li class="title-li">操作</li>
                                    <li><a href="adminAddReferee.php">添加会员</a></li>
                                    <li><a href="adminTransferPoint.php">转让运输分数</a></li>
                                    <li><a href="adminTransferRegisterPoint.php">转让注册分数</a></li>
                                    <li class="title-li">报告</li>
                                    <li><a href="adminRegisterPointReport.php">转让注册分数报告</a></li>
                                    <li><a href="adminTransferPointReport.php">转让运输分数报告</a></li>
                                    <li><a href="adminSignUpReport.php" class="mini-li">公司注册新户口报告</a></li>
                                    <li><a href="adminEditPassword.php" class="mini-li">更换密码</a></li>
                                    <li  class="last-li"><a href="logout.php">登出</a></li>
                                </ul>
							</div>
                     </div>
                <?php
                 }
                 else
                 {
                    ?>
                     <!--<div class="right-menu-div float-right">
						<!--<a href="faq.php" class="menu-padding white-to-yellow faq-padding">FAQ</a>-->
                        <!--<a  class="menu-padding white-to-yellow div-div open-login">登入</a>

 					<div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">打开菜单</button>
						<ul class="dl-menu">
                            <!--<li><a href="faq.php">FAQ</a></li>-->
                            <!--<li><a class="open-login">登入</a></li>
						</ul>
					</div>
                    </div>-->
                    <?php
                 }
            }
            ?>
        </div>

    </header>
