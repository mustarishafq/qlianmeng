-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 09, 2019 at 07:33 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_market`
--

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `id` int(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `referral_bonus` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `conversion_point` int(11) NOT NULL,
  `charges_withdraw` int(11) NOT NULL,
  `point_voucher` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`id`, `uid`, `referral_bonus`, `commission`, `conversion_point`, `charges_withdraw`, `point_voucher`) VALUES
(1, '', 20, 20, 20, 10, 100);

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(1, 'e809ed5b38535157bf9a163fa1225ade', 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 1, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:03:49', '2019-11-29 06:03:49'),
(2, 'e809ed5b38535157bf9a163fa1225ade', 'dabdeb40d42d9d281ae442fcccd93895', 'young', 1, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:03:49', '2019-11-29 06:03:49'),
(61, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '790f3b496656c24459e1ca6dab3dc912', '', 2, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-09 06:28:51', '2019-12-09 06:28:51'),
(62, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '7f1e349871e4fc3bfc0662245fb79889', '', 2, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-09 06:32:10', '2019-12-09 06:32:10');

-- --------------------------------------------------------

--
-- Table structure for table `transfer_point`
--

CREATE TABLE `transfer_point` (
  `id` int(11) NOT NULL,
  `send_uid` varchar(100) CHARACTER SET utf8 NOT NULL,
  `send_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `amount` varchar(255) CHARACTER SET utf8 NOT NULL,
  `receive_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `receive_uid` varchar(100) CHARACTER SET utf8 NOT NULL,
  `create_date` timestamp(3) NOT NULL DEFAULT current_timestamp(3),
  `status` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transfer_point`
--

INSERT INTO `transfer_point` (`id`, `send_uid`, `send_name`, `amount`, `receive_name`, `receive_uid`, `create_date`, `status`) VALUES
(1, 'e809ed5b38535157bf9a163fa1225ade', 'Martial', '550', 'roberto', 'b2743fde93dbfeae47115ef22a92d2bd', '2019-12-09 04:57:23.778', 'RECEIVED'),
(2, 'e809ed5b38535157bf9a163fa1225ade', 'Martial', '250', 'busquest', 'ef087c3d140821babc712b7094f53684', '2019-12-09 04:58:46.856', 'RECEIVED'),
(3, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '522.5', 'suarez', 'df4fe5576da5c64c8103857cd7df757b', '2019-12-09 05:00:05.600', 'RECEIVED');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` varchar(255) NOT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) DEFAULT NULL COMMENT 'Can login with email too',
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `ic_no` varchar(200) DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `epin` char(64) DEFAULT NULL,
  `salt_epin` char(64) DEFAULT NULL,
  `email_verification_code` varchar(10) DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT 1,
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT 0,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `downline_accumulated_points` decimal(50,0) NOT NULL DEFAULT 0 COMMENT 'RM1 = 100 point',
  `can_send_newsletter` tinyint(1) NOT NULL DEFAULT 0,
  `is_referred` tinyint(1) NOT NULL DEFAULT 0,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `address` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `car_model` varchar(200) DEFAULT NULL,
  `car_year` int(20) DEFAULT NULL,
  `picture_id` varchar(50) DEFAULT NULL,
  `register_downline_no` varchar(255) DEFAULT '0',
  `bonus` varchar(255) DEFAULT NULL,
  `final_amount` varchar(255) DEFAULT NULL,
  `point` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `username`, `email`, `password`, `salt`, `phone_no`, `ic_no`, `country_id`, `full_name`, `epin`, `salt_epin`, `email_verification_code`, `is_email_verified`, `is_phone_verified`, `login_type`, `user_type`, `downline_accumulated_points`, `can_send_newsletter`, `is_referred`, `date_created`, `date_updated`, `address`, `birthday`, `gender`, `bank_name`, `bank_account_holder`, `bank_account_no`, `car_model`, `car_year`, `picture_id`, `register_downline_no`, `bonus`, `final_amount`, `point`) VALUES
('790f3b496656c24459e1ca6dab3dc912', 'busquest', NULL, '2134c481c4f884e9b794fbd8f51c918234a04595433109cf44364935a6ec801a', '9732eeb022c999bcfd543c603f3042b1caf72b36', NULL, '05051616', NULL, 'Sergio Busquest', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-12-09 06:28:51', '2019-12-09 06:28:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('7f1e349871e4fc3bfc0662245fb79889', 'pique', NULL, 'd9674798f291782f08e2e7613469f548972df708b95c7e7eb07de95f5c17b33f', '7ab53a7a55e639c515bcc7a8a5ca1783fb2c243b', NULL, '0303000333', NULL, 'Gerard Pique', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-12-09 06:32:10', '2019-12-09 06:32:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', NULL, '346396240a5ba76a2b58278bedf6b350bd61da1f1ef5185ab5229694dcc8b24b', '6fe0d900fe88adbaac672b410d82963198d132da', '10101100', '10101010', NULL, 'Lionel Messi', '3666c07731b2ae30f16a0a2810a3b1634059f252de7e6ba3e3d7932ac869be2a', 'cb575a5bb774351b556b9f37e90ca611246fd857', NULL, 1, 0, 1, 1, '0', 0, 0, '2019-10-08 06:09:15', '2019-12-09 05:00:05', NULL, NULL, NULL, 'CITIBANK', 'Lionel Messi', 1010110010, NULL, NULL, NULL, '6', '500', '3500', '49477.5'),
('dabdeb40d42d9d281ae442fcccd93895', 'young', NULL, '23325e027fae92f650a4f62e5659f583f847c71fb21418b88a96f63bbc068135', 'f6961038e9a2512d5c04a57df376278051819302', '18181188', '18181818', NULL, 'Ashley Young', '4b597fdac28fd6ab069a81e7afd5c36d8f8c1208ccbecde3db4edba06341f3e0', '0545f0eab435277a668ea1c8cb80f09ca92815b0', NULL, 1, 0, 1, 1, '0', 0, 0, '2019-10-08 06:09:50', '2019-12-09 03:49:30', NULL, NULL, NULL, 'BANK KERJASAMA RAKYAT MALAYSIA BERHAD', 'Ashley Young', 1818118818, NULL, NULL, NULL, '0', '0', '3000', NULL),
('e809ed5b38535157bf9a163fa1225ade', 'Martial', '0167226357 vector hao', '33030009045ddab5244b47480e12c25eb7c8ca7db4251ae69d19fed7672cbbcb', '805949cc73f5a6e3846e737d56a1e8968dd0228a', NULL, '09091111', NULL, 'Alex Ferguson', 'e9b82448e033a782f78118c4b579febb25819639c26ba1be2bbe0230d487a280', '7ee78b49dffc463d2d34172d64e4fdf9f8e6f8f1', NULL, 1, 0, 1, 0, '0', 0, 0, '2019-10-08 06:08:10', '2019-12-09 04:58:46', NULL, NULL, NULL, 'CIMB BANK BERHAD', 'Alex Ferguson', 2147483647, NULL, NULL, NULL, '2', NULL, NULL, '99200');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referralIdReferralHistory_relateTo_userId` (`referral_id`),
  ADD KEY `referrerIdReferralHistory_relateTo_userId` (`referrer_id`),
  ADD KEY `topReferrerIdReferralHistory_relateTo_userId` (`top_referrer_id`);

--
-- Indexes for table `transfer_point`
--
ALTER TABLE `transfer_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `countryIdUser_relateTo_countryId` (`country_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `transfer_point`
--
ALTER TABLE `transfer_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD CONSTRAINT `referralIdReferralHistory_relateTo_userId` FOREIGN KEY (`referral_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `referrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `topReferrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`top_referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
