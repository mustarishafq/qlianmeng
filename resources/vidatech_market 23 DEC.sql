-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2019 at 08:20 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_market`
--

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `id` int(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `referral_bonus` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `conversion_point` int(11) NOT NULL,
  `charges_withdraw` int(11) NOT NULL,
  `point_voucher` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`id`, `uid`, `referral_bonus`, `commission`, `conversion_point`, `charges_withdraw`, `point_voucher`) VALUES
(1, '', 20, 20, 20, 10, 100);

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(3, 'e809ed5b38535157bf9a163fa1225ade', '4f57dadf446d6038e3a48fbc2fe46e4c', 'messi', 1, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-23 03:57:46', '2019-12-23 03:57:46'),
(4, 'e809ed5b38535157bf9a163fa1225ade', '36877637edaa3ffd833ebc7381ef57ef', 'ramos', 1, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-23 03:58:49', '2019-12-23 03:58:49'),
(5, 'e809ed5b38535157bf9a163fa1225ade', 'fb6fba45f9ccdb1467045870ac7144f4', 'young', 1, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-23 04:25:13', '2019-12-23 04:25:13'),
(6, 'e809ed5b38535157bf9a163fa1225ade', 'b0687cc823af71c652d21fd418b7853f', 'silva', 1, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-23 06:44:44', '2019-12-23 06:44:44');

-- --------------------------------------------------------

--
-- Table structure for table `register_point`
--

CREATE TABLE `register_point` (
  `id` int(11) NOT NULL,
  `send_uid` varchar(100) NOT NULL,
  `send_name` varchar(100) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `receive_name` varchar(100) NOT NULL,
  `receive_uid` varchar(100) NOT NULL,
  `create_date` timestamp(3) NOT NULL DEFAULT current_timestamp(3),
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `register_point`
--

INSERT INTO `register_point` (`id`, `send_uid`, `send_name`, `amount`, `receive_name`, `receive_uid`, `create_date`, `status`) VALUES
(5, 'e809ed5b38535157bf9a163fa1225ade', 'alex', '1980', 'messi', '4f57dadf446d6038e3a48fbc2fe46e4c', '2019-12-23 04:22:03.844', 'RECEIVED'),
(6, 'e809ed5b38535157bf9a163fa1225ade', 'alex', '1980', 'ramos', '36877637edaa3ffd833ebc7381ef57ef', '2019-12-23 04:23:32.669', 'RECEIVED'),
(7, 'e809ed5b38535157bf9a163fa1225ade', 'alex', '100', 'young', 'fb6fba45f9ccdb1467045870ac7144f4', '2019-12-23 04:25:47.534', 'RECEIVED'),
(8, 'e809ed5b38535157bf9a163fa1225ade', 'alex', '1000', 'silva', 'b0687cc823af71c652d21fd418b7853f', '2019-12-23 06:45:12.543', 'RECEIVED'),
(9, 'e809ed5b38535157bf9a163fa1225ade', 'alex', '1000', 'silva', 'b0687cc823af71c652d21fd418b7853f', '2019-12-23 06:46:21.116', 'RECEIVED'),
(10, 'e809ed5b38535157bf9a163fa1225ade', 'alex', '500', 'silva', 'b0687cc823af71c652d21fd418b7853f', '2019-12-23 06:46:58.337', 'RECEIVED');

-- --------------------------------------------------------

--
-- Table structure for table `transfer_point`
--

CREATE TABLE `transfer_point` (
  `id` int(11) NOT NULL,
  `send_uid` varchar(100) CHARACTER SET utf8 NOT NULL,
  `send_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `amount` varchar(255) CHARACTER SET utf8 NOT NULL,
  `commission` varchar(255) CHARACTER SET utf8 NOT NULL,
  `receive_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `receive_uid` varchar(100) CHARACTER SET utf8 NOT NULL,
  `create_date` timestamp(3) NOT NULL DEFAULT current_timestamp(3),
  `status` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` varchar(255) NOT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) DEFAULT NULL COMMENT 'Can login with email too',
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `ic_no` varchar(200) DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `epin` char(64) DEFAULT '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057',
  `salt_epin` char(64) DEFAULT '5e29f9c1c505e3e6c954240573a4c4d15c5acf93',
  `email_verification_code` varchar(10) DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT 1,
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT 0,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `downline_accumulated_points` decimal(50,0) NOT NULL DEFAULT 0 COMMENT 'RM1 = 100 point',
  `can_send_newsletter` tinyint(1) NOT NULL DEFAULT 0,
  `is_referred` tinyint(1) NOT NULL DEFAULT 0,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `address` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `picture_id` varchar(50) DEFAULT NULL,
  `register_downline_no` varchar(255) DEFAULT '0',
  `bonus` varchar(255) DEFAULT NULL,
  `final_amount` varchar(255) DEFAULT NULL,
  `point` varchar(255) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `username`, `email`, `password`, `salt`, `phone_no`, `ic_no`, `country_id`, `full_name`, `epin`, `salt_epin`, `email_verification_code`, `is_email_verified`, `is_phone_verified`, `login_type`, `user_type`, `downline_accumulated_points`, `can_send_newsletter`, `is_referred`, `date_created`, `date_updated`, `address`, `birthday`, `gender`, `picture_id`, `register_downline_no`, `bonus`, `final_amount`, `point`) VALUES
('36877637edaa3ffd833ebc7381ef57ef', 'ramos', 'ramos15@rma.cc', '60e6413e4f637b10216f4b6c9280ac142cbb3c8344724d3aec40c091845db870', 'fae952f02d947c3b2be0f3cd4b92b09bf91ef855', NULL, '15150404', NULL, 'Sergio Ramos', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, 1, 0, 1, 1, '0', 0, 1, '2019-12-23 03:58:49', '2019-12-23 04:23:32', NULL, NULL, NULL, NULL, '0', '1980', NULL, '1980'),
('4f57dadf446d6038e3a48fbc2fe46e4c', 'messi', 'messi10@barca.cc', 'f2a6f5fccf3269677b0b6315a57fd7ca4886d57ef359cf1d3360c1d5b78fed34', 'c92c2c5d87ac26e89fdd8b23f414bee17e69ada9', NULL, '1010111000', NULL, 'Lionel Messi', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, 1, 0, 1, 1, '0', 0, 1, '2019-12-23 03:57:46', '2019-12-23 04:22:03', NULL, NULL, NULL, NULL, '0', '1980', NULL, '1980'),
('b0687cc823af71c652d21fd418b7853f', 'silva', 'silva21@mc.cc', '50e86f787400fbb94eb9e392b2433168b39403c3a9b15fea6d19a1fbfaf694b1', '558f12d209d34766d61d1b4869bc6422948ae45a', NULL, '2121222111', NULL, 'David Silva', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, 1, 0, 1, 1, '0', 0, 1, '2019-12-23 06:44:44', '2019-12-23 06:46:58', NULL, NULL, NULL, NULL, '0', '4480', NULL, '0'),
('e809ed5b38535157bf9a163fa1225ade', 'saf', '0167226357 vector hao', '13bc01c8fc926571bfbe3b1d5c32ab740785ba090c11560ade1e2362413d5abf', 'af5fc9530443c889e6aea24ccf180b86c9fdefba', NULL, '09091111', NULL, 'Alex Ferguson', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, 1, 0, 1, 0, '0', 0, 0, '2019-10-08 06:08:10', '2019-12-23 07:19:53', NULL, NULL, NULL, NULL, '0', '5520', NULL, '12376'),
('fb6fba45f9ccdb1467045870ac7144f4', 'young', 'young18@mu.cc', '76f979e2b6723c43f28913080225f282ccf8792da0dd614c061b946ddf733aec', '3591d47579024ecc4fdbe7ebda8838fa009394b7', NULL, '1818111888', NULL, 'Ashley Young', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, 1, 0, 1, 1, '0', 0, 1, '2019-12-23 04:25:13', '2019-12-23 04:25:47', NULL, NULL, NULL, NULL, '0', '100', NULL, '1980');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referralIdReferralHistory_relateTo_userId` (`referral_id`),
  ADD KEY `referrerIdReferralHistory_relateTo_userId` (`referrer_id`),
  ADD KEY `topReferrerIdReferralHistory_relateTo_userId` (`top_referrer_id`);

--
-- Indexes for table `register_point`
--
ALTER TABLE `register_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfer_point`
--
ALTER TABLE `transfer_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `countryIdUser_relateTo_countryId` (`country_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `register_point`
--
ALTER TABLE `register_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `transfer_point`
--
ALTER TABLE `transfer_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD CONSTRAINT `referralIdReferralHistory_relateTo_userId` FOREIGN KEY (`referral_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `referrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `topReferrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`top_referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
