-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 09, 2019 at 10:30 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_market`
--

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `id` int(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `referral_bonus` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `conversion_point` int(11) NOT NULL,
  `charges_withdraw` int(11) NOT NULL,
  `point_voucher` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`id`, `uid`, `referral_bonus`, `commission`, `conversion_point`, `charges_withdraw`, `point_voucher`) VALUES
(1, '', 20, 20, 20, 10, 100);

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(1, 'e809ed5b38535157bf9a163fa1225ade', 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', 1, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:03:49', '2019-11-29 06:03:49'),
(2, 'e809ed5b38535157bf9a163fa1225ade', 'dabdeb40d42d9d281ae442fcccd93895', 'young', 1, 'e809ed5b38535157bf9a163fa1225ade', '2019-11-29 06:03:49', '2019-11-29 06:03:49'),
(65, 'e809ed5b38535157bf9a163fa1225ade', '32062e58717eb5214ec9e966ac404865', 'ramos', 1, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-09 09:02:27', '2019-12-09 09:02:27'),
(66, 'a1a2e99ddbdd69b25d8cfa08b2af049e', '9e6344acbae03e21e0ac2e43025956ef', 'busquest', 2, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-09 09:11:44', '2019-12-09 09:11:44'),
(67, '9e6344acbae03e21e0ac2e43025956ef', '72e34590e01b5687acd0a14551b07f72', 'dejong', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-09 09:12:31', '2019-12-09 09:12:31'),
(68, 'e809ed5b38535157bf9a163fa1225ade', '35e8bd23a5712e4c0d48ffe32b3d2e9c', 'silva', 1, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-09 09:26:55', '2019-12-09 09:26:55');

-- --------------------------------------------------------

--
-- Table structure for table `transfer_point`
--

CREATE TABLE `transfer_point` (
  `id` int(11) NOT NULL,
  `send_uid` varchar(100) CHARACTER SET utf8 NOT NULL,
  `send_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `amount` varchar(255) CHARACTER SET utf8 NOT NULL,
  `receive_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `receive_uid` varchar(100) CHARACTER SET utf8 NOT NULL,
  `create_date` timestamp(3) NOT NULL DEFAULT current_timestamp(3),
  `status` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transfer_point`
--

INSERT INTO `transfer_point` (`id`, `send_uid`, `send_name`, `amount`, `receive_name`, `receive_uid`, `create_date`, `status`) VALUES
(5, 'e809ed5b38535157bf9a163fa1225ade', 'Martial', '550', 'messi', 'a1a2e99ddbdd69b25d8cfa08b2af049e', '2019-12-09 09:13:31.603', 'RECEIVED'),
(6, 'a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', '522.5', 'busquest', '9e6344acbae03e21e0ac2e43025956ef', '2019-12-09 09:14:19.801', 'RECEIVED'),
(7, '9e6344acbae03e21e0ac2e43025956ef', 'busquest', '190', 'dejong', '72e34590e01b5687acd0a14551b07f72', '2019-12-09 09:14:57.245', 'RECEIVED'),
(8, 'e809ed5b38535157bf9a163fa1225ade', 'Martial', '800', 'young', 'dabdeb40d42d9d281ae442fcccd93895', '2019-12-09 09:23:41.827', 'RECEIVED');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` varchar(255) NOT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) DEFAULT NULL COMMENT 'Can login with email too',
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `ic_no` varchar(200) DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `epin` char(64) DEFAULT '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057',
  `salt_epin` char(64) DEFAULT '5e29f9c1c505e3e6c954240573a4c4d15c5acf93',
  `email_verification_code` varchar(10) DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT 1,
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT 0,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `downline_accumulated_points` decimal(50,0) NOT NULL DEFAULT 0 COMMENT 'RM1 = 100 point',
  `can_send_newsletter` tinyint(1) NOT NULL DEFAULT 0,
  `is_referred` tinyint(1) NOT NULL DEFAULT 0,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `address` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `car_model` varchar(200) DEFAULT NULL,
  `car_year` int(20) DEFAULT NULL,
  `picture_id` varchar(50) DEFAULT NULL,
  `register_downline_no` varchar(255) DEFAULT '0',
  `bonus` varchar(255) DEFAULT NULL,
  `final_amount` varchar(255) DEFAULT NULL,
  `point` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `username`, `email`, `password`, `salt`, `phone_no`, `ic_no`, `country_id`, `full_name`, `epin`, `salt_epin`, `email_verification_code`, `is_email_verified`, `is_phone_verified`, `login_type`, `user_type`, `downline_accumulated_points`, `can_send_newsletter`, `is_referred`, `date_created`, `date_updated`, `address`, `birthday`, `gender`, `bank_name`, `bank_account_holder`, `bank_account_no`, `car_model`, `car_year`, `picture_id`, `register_downline_no`, `bonus`, `final_amount`, `point`) VALUES
('32062e58717eb5214ec9e966ac404865', 'ramos', NULL, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '04041515', NULL, 'Sergio Ramos', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, 1, 0, 1, 1, '0', 0, 1, '2019-12-09 09:02:27', '2019-12-09 09:20:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('35e8bd23a5712e4c0d48ffe32b3d2e9c', 'silva', NULL, '1c88819ed37417f6f0bd66e39fd965b1ce1143d65375f17740c2348a037708ba', 'ff0beb260b219636c02bb7f744537cd8fc50cca0', NULL, '212120102121', NULL, 'David Silva', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, 1, 0, 1, 1, '0', 0, 1, '2019-12-09 09:26:55', '2019-12-09 09:26:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL),
('72e34590e01b5687acd0a14551b07f72', 'dejong', NULL, 'e9a20f2fe54d15a4a3a066a500237d408f88a0aceb9855cbd4c222de7df081e3', '93b194ded6508b50b058dd8160a81f073f7c23af', NULL, '2121222111', NULL, 'Frankie de Jong', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, 1, 0, 1, 1, '0', 0, 1, '2019-12-09 09:12:31', '2019-12-09 09:14:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '190'),
('9e6344acbae03e21e0ac2e43025956ef', 'busquest', NULL, 'e6d34a1bf0c5491543055f753b4b9ca92f8db676e5b98fdc8d633ea9734b5300', 'f6a953fe85ba42f238e60fd510f8b9c8afdd4b7a', NULL, '05051616', NULL, 'Sergio Busquest', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, 1, 0, 1, 1, '0', 0, 1, '2019-12-09 09:11:44', '2019-12-09 09:14:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '332.5'),
('a1a2e99ddbdd69b25d8cfa08b2af049e', 'messi', NULL, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '10101100', '10101010', NULL, 'Lionel Messi', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, 1, 0, 1, 1, '0', 0, 0, '2019-10-08 06:09:15', '2019-12-09 09:23:14', NULL, NULL, NULL, 'CITIBANK', 'Lionel Messi', 1010110010, NULL, NULL, NULL, '6', '500', '3500', '5027.5'),
('dabdeb40d42d9d281ae442fcccd93895', 'young', NULL, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '18181188', '18181818', NULL, 'Ashley Young', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, 1, 0, 1, 1, '0', 0, 0, '2019-10-08 06:09:50', '2019-12-09 09:23:41', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '0', '0', '3000', '800'),
('e809ed5b38535157bf9a163fa1225ade', 'Martial', '0167226357 vector hao', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, '09091111', NULL, 'Alex Ferguson', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, 1, 0, 1, 0, '0', 0, 0, '2019-10-08 06:08:10', '2019-12-09 09:23:41', NULL, NULL, NULL, 'CIMB BANK BERHAD', 'Alex Ferguson', 2147483647, NULL, NULL, NULL, '2', NULL, NULL, '8650');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referralIdReferralHistory_relateTo_userId` (`referral_id`),
  ADD KEY `referrerIdReferralHistory_relateTo_userId` (`referrer_id`),
  ADD KEY `topReferrerIdReferralHistory_relateTo_userId` (`top_referrer_id`);

--
-- Indexes for table `transfer_point`
--
ALTER TABLE `transfer_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `countryIdUser_relateTo_countryId` (`country_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `transfer_point`
--
ALTER TABLE `transfer_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD CONSTRAINT `referralIdReferralHistory_relateTo_userId` FOREIGN KEY (`referral_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `referrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `topReferrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`top_referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
