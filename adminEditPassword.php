<?php
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/sessionLoginChecker.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

?>

<!doctype html>
<html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/adminEditPassword.php" />
    <meta property="og:title" content="更换密码 | Q联盟"/>
    <title>更换密码 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/adminEditPassword.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">

<!-- Start Menu -->
<?php include 'header-sherry.php'; ?>
<div class="yellow-body padding-from-menu same-padding">
	<h1 class="success-h1 text-center">
        更换密码
    </h1>
    <div class="reset-password-div">
        <form class="login-form" method="POST" action="utilities/adminChangePasswordFunction.php">
            <div class="input-grey-div">
                <span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="现有密码" title="现有密码"></span>
                <input name="adminEditPassword_current" id="adminEditPassword_current" required class="login-input password-input clean" type="password" placeholder="现有密码">
                    <span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_current_img"></span>
            </div>
            
            <div class="input-grey-div">
                <span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="新密码" title="新密码"></span>
                <input name="adminEditPassword_new" id="adminEditPassword_new" required class="login-input password-input clean" type="password" placeholder="新密码">
                 <span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_new_img"></span>
            </div>        
            <div class="input-grey-div">
                <span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="重新输入新密码" title="重新输入新密码"></span>
                <input name="adminEditPassword_reenter" id="adminEditPassword_reenter" required class="login-input password-input clean" type="password" placeholder="重新输入新密码">
                <span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_reenter_img"></span>
            </div>
            <div class="clear"></div>
                <div class="three-btn-container">
                    <button class="shipout-btn-a black-button three-btn-a clean style-button"><b>确认</b></button>
                </div>   
        </form>

     </div>


</div>
<?php include 'js.php'; ?>

<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "现有密码不匹配  <br>    请再试一遍 ！";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "新密码必须超过5个字符";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "新密码与重新输入新密码不匹配    <br>  请再试一遍 ！";
        }
        if($_GET['type'] == 4)
        {
            $messageType = "服务器问题  <br>  请稍后再试一遍 ！";
        }
        if($_GET['type'] == 5)
        {
            $messageType = "成功更换密码";
        }
        echo '
        <script>
            putNoticeJavascript("通告 !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
  viewPassword( document.getElementById('editPassword_current_img'), document.getElementById('adminEditPassword_current'));
  viewPassword( document.getElementById('editPassword_new_img'), document.getElementById('adminEditPassword_new'));
  viewPassword( document.getElementById('editPassword_reenter_img'), document.getElementById('adminEditPassword_reenter'));
</script>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Wrong Code Verification. <br>Please Try Again.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Password must be more than 5. <br>Please Try Again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Password Does Not Match. <br>Please Try Again";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Server Failure ! <br>Please Try Again Later In A Few Minutes.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
</html>