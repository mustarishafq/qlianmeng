<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/adminTransferRegisterPoint.php" />
    <meta property="og:title" content="公司转让注册分数 | Q联盟" />
    <title>公司转让注册分数 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/adminTransferRegisterPoint.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

  <h1 class="username">转让注册分数</h1>
  <h3>分数 : <?php echo $userRefereeLimit = $userDetails->getBonus(); ?>分</h3>

    <form method="POST" action="utilities/adminRegisterPointsFunction.php" >

    <div class="input-grey-div">
        <div class="left-points-div underline-div">
            <span class="input-span"><img src="img/coin.png" class="login-input-icon" alt="分数" title="分数"></span>
            <input class="login-input name-input clean" type="number" id="transferAmount" name="transferAmount" placeholder="转让分数">
        </div>
    </div>

    <div class="input-grey-div">
        <div class="left-points-div underline-div">
            <span class="input-span"><img src="img/user.png" class="login-input-icon" alt="用户名" title="用户名"></span>
            <input class="login-input name-input clean" type="text" id="username" name="username" placeholder="用户名">
        </div>
    </div>

    <!-- <input type="hidden" id="withdraw_epin_convert" name="withdraw_epin_convert" value="123321"> -->

    <div class="clear"></div>

    <button class="yellow-button clean" type="submit">转让</button>

    </form>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>


<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "成功转让注册分数！";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "服务器失灵";
        }
        else if($_GET['type'] == 3)
        {
            // $messageType = "转让注册分数失败！";
            $messageType = "成功转让注册分数！";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "转让用户的的资料不符合！";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "转让用户的组织不相同！";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "分数不足够！";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "转让用户不存在！";
        }
        
        echo '
        <script>
            putNoticeJavascript("通告 !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>


</body>
</html>
