<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/editProfile.php" />
    <meta property="og:title" content="修改资料 | Q联盟" />
    <title>修改资料 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/editProfile.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
<form method="POST" onsubmit="return editprofileFunc(name);" action="utilities/editProfileFunction.php">

    <div class="edit-profile-div2">
        <h1 class="username"> <?php echo $userDetails->getUsername();?> </h1>
        <!-- <h2 class="profile-title">BASIC INFORMATION</h2> -->
        <h2 class="profile-title"><?php echo _MAINJS_PROFILE_BASIC_INFORMATION ?></h2>
        <table class="edit-profile-table">

        <?php   $fullName = $userDetails->getFullname();
        if ($fullName)
        {
        ?>

        	<tr class="profile-tr">
                <!-- <td class="profile-td1">IC Number</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_IC_NUMBER ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getIcNo();?></td>
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Username</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_USERNAME ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getUsername();?></td>
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Full Name</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_FULLNAME ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getFullname();?></td>
            </tr>

        <?php
        }
        ?>

        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Birthday</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_BIRTHDAY ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_birthday" class="clean edit-profile-input" type="text" placeholder="YYYY-MM-DD" value="<?php echo $userDetails->getBirthday();?>" name="update_birthday"></td>
            </tr>

        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Gender </td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_GENDER ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                	<select class="edit-profile-input edit-profile-select clean" id="update_gender" value="<?php echo $userDetails->getGender();?>" name="update_gender">

                        <?php
                            if($userDetails->getGender() == '')
                            {
                                ?>
                                <option value="Female"  name='Female'>Female</option>
                                <option value="Male"  name='male'>Male</option>
                                <option selected value=""  name=''></option>
                                <?php
                            }
                            else if($userDetails->getGender() == 'Male')
                            {
                                ?>
                                <option value="Female"  name='Female'>Female</option>
                                <option selected value="Male"  name='male'>Male</option>
                                <?php
                            }
                            else if($userDetails->getGender() == 'Female')
                            {
                                ?>
                                <option selected value="Female"  name='Female'>Female</option>
                                <option value="Male"  name='male'>Male</option>
                                <?php
                            }
                        ?>

                    </select><img src="img/dropdown2.png" class="dropdown-png">
                </td>
            </tr>
        </table>

        <!-- <h2 class="profile-title">CONTACT INFORMATION</h2> -->
        <h2 class="profile-title"><?php echo _MAINJS_PROFILE_CONTACT_INFORMATION ?></h2>
        <table class="edit-profile-table">
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Phone</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_PHONE ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_phoneno" class="clean edit-profile-input" type="text" value="<?php echo $userDetails->getPhoneNo();?>" name="update_phoneno" required></td>
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Address</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_ADDRESS ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_address" class="clean edit-profile-input" type="text" placeholder="" value="<?php echo $userDetails->getAddress();?>" name="update_address"></td>
            </tr>
        </table>
        <button input type="submit" name="submit" value="Submit" class="confirm-btn text-center white-text clean black-button"><?php echo _MAINJS_EDIT_PROFILE_CONFIRM ?></button>
        <p class="change-password-p"><a href="editPassword.php" class="edit-password-a black-link"><?php echo _MAINJS_EDIT_PROFILE_EDIT_PASSWORD ?></a></p>

    </div>
</form>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Error";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Fail To Update Data.";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Data Update Successfully.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>
