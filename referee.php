<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';



$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];


$userRows2 = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/referee.php" />
    <meta property="og:title" content="我的下线 | Q联盟" />
    <title>我的下线 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/referee.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <div class="right-profile-div">
    	<div class="profile-tab width100">
        	<a href="profile.php" class="profile-tab-a">个人中心</a>
            <a href="#" class="profile-tab-a active-tab-a">我的组织</a>
        </div>

        <div class="width100 oveflow white-text">
            <?php
                $conn = connDB();
                    if($getWho){

                          echo '<ul>';
                          $lowestLevel = $getWho[0]->getCurrentLevel();
                          foreach($getWho as $thisPerson){
                              $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                              $thisTempUser = $tempUsers[0];

                              if($thisPerson->getCurrentLevel() == $lowestLevel)
                              {
                                  echo '<li id="'.$thisPerson->getReferralId().'">'.$thisTempUser->getUsername().'</li>';
                              }
                            }

                          if (isset($_POST["mybutton"]))
                          {


                        echo '<ul>';
                        $lowestLevel = $getWho[0]->getCurrentLevel();
                        foreach($getWho as $thisPerson){
                            $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                            $thisTempUser = $tempUsers[0];

                                echo '
                                    <script type="text/javascript">
                                        var div = document.getElementById("'.$thisPerson->getReferrerId().'");

                                        div.innerHTML += "<ul name=\'ul-'.$thisPerson->getReferrerId().'\'><li id=\''.$thisPerson->getReferralId().'\'>'.$thisTempUser->getUsername().'</li></ul>";
                                    </script>
                                ';

                        }
                        echo '</ul>';

                    }elseif (isset($_POST["mybutton2"])) {


                    }
                }
            ?>
       </div>

        <form action="" method="POST">
            <button input type="submit" class="confirm-btn text-center clean black-button anc-ow-btn same-width-btn same-width-btn1" name="mybutton"><?php echo _MAINJS_REFEREE_EXPAND_ALL ?></button>
            <button input type="submit" class="confirm-btn text-center clean black-button anc-ow-btn same-width-btn same-width-btn2" name="mybutton2"><?php echo _MAINJS_REFEREE_COLLAPSE_ALL ?></button>
        </form>

        

	</div>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>
