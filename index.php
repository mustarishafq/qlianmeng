<?php
if (session_id() == ""){
    session_start();
}

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';


function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://qlianmeng.asia/" />
<meta property="og:title" content="Q联盟" />
<title>Q联盟</title>
<meta property="og:description" content="Q联盟" />
<meta name="description" content="Q联盟" />
<meta name="keywords" content="Q联盟, League Q,etc">
<link rel="canonical" href="https://qlianmeng.asia/" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">

	<?php include 'header-sherry.php'; ?>

<div class="index-body width100">
	
<?php
  $oilxag_uname = null;
  $oilxag_password = null;

	$uid = $_SESSION['uid'];

if(isset($_COOKIE['remember-oilxag']) && isset($_COOKIE['email-oilxag']) && isset($_COOKIE['password-oilxag']))
{
  echo'saddasassadsaddsasadsa';

    echo $oilxag_uname = $_COOKIE['email-oilxag'];
    echo $oilxag_password = $_COOKIE['password-oilxag'];
}
else
{
  echo $oilxag_uname = '';
  echo $oilxag_password = '';
}


?>
	<div class="login-div same-padding">
        <h1 class="white-h1">登入</h1>
        <form class="login-form" method="POST" action="utilities/loginFunction.php">
            <div class="input-grey-div">
                <span class="input-span"><img src="img/user.png" class="login-input-icon" alt="用户名" title="用户名"></span>
                <input class="login-input name-input clean" type="text" placeholder="用户名" name="email" value="<?php echo $oilxag_uname;?>">
            </div>
            <div class="input-grey-div">
                <span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="密码" title="密码"></span>
                <input class="login-input password-input clean" type="password" placeholder="密码" name="password" id="login_password" value="<?php echo $oilxag_password;?>">
                <span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="login_view_password"></span>
            </div>
            <?php
            if(isset($_COOKIE['remember-oilxag']) && $_COOKIE['remember-oilxag'] == 1)
            {
              ?>
                <div class="left-check"><input checked type="checkbox" class="checkbox-remember-me" name="remember-me"> <p class="remember-me-p"> 记住我</p></div>
              <?php
            }
            else
            {
              ?>
                <div class="left-check"><input type="checkbox" class="checkbox-remember-me" name="remember-me"> <p class="remember-me-p"> 记住我</p></div>
              <?php
            }
            ?>
            <div class="right-forgot"><a class="open-forgot forgot-password-a">忘记密码?</a></div>
            <div class="clear"></div>
            <button class="yellow-button clean" name="loginButton" type="submit">登入</button>
        </form>    
 	</div>   
</div>
<div class="clear"></div>

<!-- CSS -->
<style>
.food-gif{
	width:100px;
	position:absolute;
	top:calc(50% - 150px);
	text-align:center;
}
.center-food{
	width:100%;
	text-align:center;
	margin-left:-50px;}
#container{
	margin-top:-20px;}
#overlay{
  position:fixed;
  z-index:99999;
  top:0;
  left:0;
  bottom:0;
  right:0;
  background:#7cd1d1;
  /*background: -moz-linear-gradient(left, #a9151c 0%, #d60d26 100%);
  background: -webkit-linear-gradient(left, #a9151c 0%,#d60d26 100%);
  background: linear-gradient(to right, #a9151c 0%,#d60d26 100%);*/
  transition: 1s 0.4s;
}
#progress{
  height:1px;
  background:#fff;
  position:absolute;
  width:0;
  top:50%;
}
#progstat{
  font-size:0.7em;
  letter-spacing: 3px;
  position:absolute;
  top:50%;
  margin-top:-40px;
  width:100%;
  text-align:center;
  color:#fff;
}
@media all and (max-width: 500px){
.food-gif{
	width:60px;
	top:calc(50% - 120px);
	text-align:center;
}
.center-food{
	margin-left:-30px;}	

}
</style>

<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_email_button'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "sherry2.vidatech@gmail.com, enquiry@qlianmeng.asia";
    $email_subject = "Contact Form via Q Lian Meng";
 
    function died($error) 
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
	
     
 
    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments']; 
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;
	
	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }
 
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }
 


 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";   
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
echo '<script>alert("Thank you! We will be in contact shortly!")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php
 
}
?>
<?php include 'js.php'; ?>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            // $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
            $messageType = "Register Successfully!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            // $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
            $messageType = "Register Successfully!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong email format.";
        }
        else if($_GET['type'] == 7)
        {
            // $messageType = "There are no user with this email ! Please try again.";
            $messageType = "此用户不存在！";
        }
        else if($_GET['type'] == 8)
        {
            // $messageType = "Successfully reset your password! Please check your email.";
            $messageType = "验证码已发送，请查看您的电邮。";
        }
        else if($_GET['type'] == 9)
        {
            $messageType = "成功重设密码！";
        }
        else if($_GET['type'] == 10)
        {
            $messageType = "Please confirm your registration inside your email! ";
        }
        else if($_GET['type'] == 11)
        {
            // $messageType = "Incorrect email or password! ";
            $messageType = "错误的用户名或密码";
        }
        echo '
        <script>
            putNoticeJavascript("通告 !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("通告 !! ","'.$messageType.'");
    </script>
    ';   
}
?>
<script src="js/jssor.slider.min.js" type="text/javascript"></script>
<script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 3000;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    
    
    <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- #endregion Jssor Slider End -->

</body>
</html>